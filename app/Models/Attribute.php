<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Attribute extends Model
{
    use HasFactory;

    protected $fillable = ['chat_id', 'key', 'valueable_id', 'valueable_type'];

    public function chat() {
        return $this->belongsTo(Chat::class);
    }

    public function valueable() {
        return $this->morphTo(__FUNCTION__, 'valueable_type', 'valueable_id');
    }
}
