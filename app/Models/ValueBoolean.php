<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ValueBoolean extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $casts = [
        'value' => 'boolean'
    ];

    protected $fillable = ['value'];

    protected function attributes() {
        return $this->morphMany(Attribute::class,'valueable');
    }
}
