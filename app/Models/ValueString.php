<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ValueString extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable = ['value'];

    protected function attributes() {
        return $this->morphMany(Attribute::class,'valueable');
    }
}
