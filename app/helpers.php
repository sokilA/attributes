<?php

use Illuminate\Database\Eloquent\Relations\Relation;

if(!function_exists('responseApiMessage')){
    function responseApiMessage($success = true, $message = null){
        return response()->json(array(
            'success' => !!$success,
            'message' => $message,
        ), ($success ? 200 : 422));
    }
}

if(!function_exists('createFromMorphMapByType')){
    function createFromMorphMapByType($type = true, $data = null){
        return Relation::getMorphedModel($type)::create($data);
    }
}