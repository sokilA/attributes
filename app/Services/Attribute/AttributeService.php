<?php


namespace App\Services\Attribute;


use App\Models\Attribute;
use App\Repositories\AttributeRepository\AttributeRepositoryInterface;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\Facades\DB;

class AttributeService implements AttributeServiceInterface
{
    /**
     * @var
     */
    private $repository;

    public function __construct(AttributeRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function create(array $data)
    {
        $value = createFromMorphMapByType($data['type'], [
            'value' => $data['value']
        ]);

        $attribute = new Attribute();
        $attribute->chat_id = $data['chat_id'];
        $attribute->key = $data['key'];
        $attribute->valueable()->associate($value);
        $attribute->save();

        return $attribute;
    }

    public function update(array $data, Attribute $attribute)
    {
        $attribute->key = $data['key'];

        if($attribute->valueable_type === $data['type']){
            $attribute->valueable->value = $data['value'];
            $attribute->valueable->save();
        } else {
            $attribute->valueable()->delete();
            $value = createFromMorphMapByType($data['type'], [
                'value' => $data['value']
            ]);
            $attribute->valueable()->associate($value);
        }

        $attribute->save();

        return $attribute;
    }

    public function delete(Attribute $attribute)
    {
        // can move this logic in to observer
        $attribute->valueable()->delete();

        return $attribute->delete();
    }
}