<?php


namespace App\Services\Attribute;


use App\Models\Attribute;

interface AttributeServiceInterface
{
    public function create(array $data);
    public function update(array $data, Attribute $attribute);
    public function delete(Attribute $attribute);
}