<?php

namespace App\Providers;

use App\Models\ValueBoolean;
use App\Models\ValueDatetime;
use App\Models\ValueDecimal;
use App\Models\ValueInteger;
use App\Models\ValueString;
use App\Repositories\AttributeRepository\AttributeRepository;
use App\Repositories\AttributeRepository\AttributeRepositoryInterface;
use App\Services\Attribute\AttributeService;
use App\Services\Attribute\AttributeServiceInterface;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Relations\Relation;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(AttributeServiceInterface::class, AttributeService::class);

        Relation::morphMap([
            'integer' => ValueInteger::class,
            'string' => ValueString::class,
            'decimal' => ValueDecimal::class,
            'boolean' => ValueBoolean::class,
            'datetime' => ValueDatetime::class,
        ]);
    }
}
