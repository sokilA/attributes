<?php

namespace App\Http\Controllers;

use App\Models\Chat;
use Illuminate\Http\Request;

class ChatController extends Controller
{
    // In future need to add pagination and create separate method. It just for test task
    public function __invoke()
    {
        return response()->json(
            Chat::withCount('attributes')->get()
        );
    }
}
