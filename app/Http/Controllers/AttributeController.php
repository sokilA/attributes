<?php

namespace App\Http\Controllers;

use App\Http\Requests\AttributeRequest;
use App\Http\Resources\AttributeCollectionResource;
use App\Http\Resources\AttributeResource;
use App\Models\Attribute;
use App\Models\Chat;
use App\Services\Attribute\AttributeServiceInterface;

class AttributeController extends Controller
{
    private $attributeService;

    public function __construct(AttributeServiceInterface $attributeService) {
        $this->attributeService = $attributeService;
    }

    /**
     *
     * @param  Chat  $chat
     * @return AttributeCollectionResource
     */
    public function index(Chat $chat)
    {
        return new AttributeCollectionResource($chat->attributes);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  AttributeRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(AttributeRequest $request)
    {
        $this->attributeService->create($request->validated());
        return responseApiMessage(true, 'Created');
    }

    /**
     * Display the specified resource.
     *
     * @param  Chat  $chat
     * @param  Attribute  $attribute
     * @return AttributeResource
     */
    public function show(Chat $chat, Attribute $attribute)
    {
        return new AttributeResource($attribute);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  AttributeRequest  $request
     * @param  Chat  $chat
     * @param  Attribute  $attribute
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(AttributeRequest $request, Chat $chat, Attribute $attribute)
    {
        $this->attributeService->update($request->validated(), $attribute);
        return responseApiMessage(true, 'Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Chat  $chat
     * @param  Attribute  $attribute
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Chat $chat, Attribute $attribute)
    {
        $this->attributeService->delete($attribute);
        return responseApiMessage(true, 'Deleted');
    }
}
