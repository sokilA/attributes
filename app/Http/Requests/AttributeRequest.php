<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Validator;

class AttributeRequest extends FormRequest
{
    protected $types = [
        'integer',
        'string',
        'decimal',
        'boolean',
        'datetime'
    ];
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function prepareForValidation()
    {
        $this->merge([
            'chat_id' => $this->route('chat'),
            'type' => $this->request->get('type')
        ]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'chat_id' => 'required',
            'key' => [
                'required',
                Rule::unique('attributes')->where(function ($query) {
                    return $query->where('chat_id', $this->route('chat'));
                })
            ],
            'type' => [
                'required',
                Rule::in($this->types)
            ],
            'value' => 'required'
        ];
    }
}
