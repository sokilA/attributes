<?php


namespace App\Repositories\AttributeRepository;


interface AttributeRepositoryInterface
{
    public function getByChatId(int $id);
    public function getById(int $id);
}