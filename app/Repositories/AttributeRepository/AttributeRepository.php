<?php


namespace App\Repositories\AttributeRepository;


use App\Models\Attribute;

class AttributeRepository implements AttributeRepositoryInterface
{
    public function getByChatId(int $chat_id)
    {
        // If will be a lot of attributes need to make pagination
        return Attribute::where('chat_id', $chat_id)->orderByDesc('id')->get();
    }

    public function getById(int $id)
    {
        return Attribute::findOrFail($id);
    }
}