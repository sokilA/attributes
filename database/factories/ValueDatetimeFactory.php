<?php

namespace Database\Factories;

use App\Models\ValueDatetime;
use Illuminate\Database\Eloquent\Factories\Factory;

class ValueDatetimeFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ValueDatetime::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'value' => $this->faker->dateTime
        ];
    }
}
