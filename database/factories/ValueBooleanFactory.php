<?php

namespace Database\Factories;

use App\Models\ValueBoolean;
use Illuminate\Database\Eloquent\Factories\Factory;

class ValueBooleanFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ValueBoolean::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'value' => $this->faker->boolean
        ];
    }
}
