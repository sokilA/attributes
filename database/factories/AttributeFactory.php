<?php

namespace Database\Factories;

use App\Models\Attribute;
use App\Models\ValueBoolean;
use App\Models\ValueDatetime;
use App\Models\ValueDecimal;
use App\Models\ValueInteger;
use App\Models\ValueString;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Arr;

class AttributeFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Attribute::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $types = [
            ValueInteger::class,
            ValueDecimal::class,
            ValueString::class,
            ValueBoolean::class,
            ValueDatetime::class,
        ];

        $value = Arr::random( $types )::factory()->create();

        return [
            'key' => $this->faker->word,
            'valueable_type' => $value->getMorphClass(),
            'valueable_id' => $value->getKey()
        ];
    }
}
