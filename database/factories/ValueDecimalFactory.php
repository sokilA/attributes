<?php

namespace Database\Factories;

use App\Models\ValueDecimal;
use Illuminate\Database\Eloquent\Factories\Factory;

class ValueDecimalFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ValueDecimal::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'value' => $this->faker->randomFloat(2, 1, 100 )
        ];
    }
}
