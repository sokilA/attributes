import Vue from 'vue';
import VueRouter from 'vue-router';

import Home from './pages/Home.vue';
import Attributes from './pages/Attributes'
import Attribute from './pages/Attribute'

Vue.use(VueRouter);

const router = new VueRouter({
	mode: 'history',
	linkExactActiveClass: 'active',
	routes: [
		{
			path: '/',
			name: 'home',
			component: Home
		},
		{
			path: '/attributes/:id',
			name: 'attributes',
			component: Attributes
		},
		{
			path: '/attribute/:chat_id/:id?',
			name: 'attribute',
			component: Attribute
		},
	]
});

export default router;