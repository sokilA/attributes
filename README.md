## Installation

Clone project from bitbucket and change .env.example to .env

After cloning need to run docker build.

```bash
docker compose up --build
```

When container will be created need to run exec the docker

```bash
docker exec -it {container name} bash
```

After that need to install composer packages and node modules

```bash
composer install
npm i
```

Next step need to compiled CSS and JavaScript assets

```bash
npm run prod
```

The last step to run migration and seed.

```bash
php artisan migrate
php artisan db:seed --class=ChatSeeder
```

For future using of project needed to run only up and down command

```bash
docker compose up
docker compose down
```